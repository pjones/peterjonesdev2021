import React from 'react';

export default function bannerScroll() {
	const [scroll, setScroll] = useState('');

	useEffect(() => {
		window.scrollTo(0, 0);
		window.addEventListener('scroll', handleScroll);
		return () => window.removeEventListener('scroll', handleScroll);
	}, []);

	function handleScroll() {
		let scroll = window.scrollY;
		setScroll(scroll);
	}
	return <div></div>;
}
