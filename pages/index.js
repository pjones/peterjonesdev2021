import Head from 'next/head';
import Link from 'next/link';
import styles from '../styles/Home.module.css';
import Banner from '../components/page-banners/HomepageBanner';
import { connectToDatabase } from '../util/mongodb';

const Home = ({ isConnected, properties }) => {
	return (
		<>
			<Head>
				<title>Peter Jones | Web Developer</title>
			</Head>
			<Banner />
			<main role='main' className='content' style={{ minHeight: '2000px' }}>
				<h1 className={styles.title}>Peter Jones | Web Developer</h1>
				{isConnected ? (
					<h2 className='subtitle'>You are connected to MongoDB</h2>
				) : (
					<h2 className='subtitle'>
						You are NOT connected to MongoDB. Check the <code>README.md</code>{' '}
						for instructions.
					</h2>
				)}
				<p className={styles.text}>
					Lorem ipsum dolor sit amet cconsectetur adipisicing elit. A ipsa odit,
					impedit neque distinctio quos molestias est sunt. Quae natus aliquam,
					atque iste pariatur repellat delectus labore praesentium enim
					deserunt, eveniet voluptas debitis, nihil aut totam hic? Explicabo,
					repellendus dicta?
				</p>
				<p className={styles.text}>
					Lorem ipsum dolor sit amet consectetur adipisicing elit. Sed sequi
					dolorem, voluptatibus quia aliquid eos dolore nesciunt. Expedita optio
					consequatur amet, molestiae recusandae nemo aspernatur eos, ducimus
					voluptate eum in!
				</p>
				<Link href='/code'>
					<a className={styles.btn}>Code</a>
				</Link>
				<div>
					<ul>
						{properties &&
							properties.map(property => (
								<li key={property.id} className='users'>
									Name: {property.name}
									<br />
									Email: {property.email}
								</li>
							))}
					</ul>
				</div>
			</main>
		</>
	);
};

export async function getServerSideProps({ req }) {
	const { context } = req.netlifyFunctionParams || {};
	const { client } = await connectToDatabase();
	const isConnected = await client.isConnected();

	if (context) {
		console.log('Setting callbackWaitsForEmptyEventLoop: false');
		context.callbackWaitsForEmptyEventLoop = false;
	}

	const { db } = await connectToDatabase();
	const data = await db
		.collection('users')
		.find()
		.sort({ _id: 1 })
		.limit(10)
		.toArray();
	console.log(data);

	const properties = data.map(property => {
		const id = JSON.parse(JSON.stringify(property._id));
		return {
			id,
			name: property.name,
			email: property.email
		};
	});

	console.log('isConneccted', isConnected);

	return {
		props: { isConnected, properties }
	};
}

export default Home;
