import Router from 'next/router';
import NProgress from 'nprogress';
import { Provider } from 'next-auth/client';
import Layout from '../components/Layout';
import '../styles/globals.scss';

NProgress.configure({
	easing: 'ease-in-out',
	speed: 350,
	showSpinner: false,
	trickleRate: 1.02,
	trickleSpeed: 800
});

Router.events.on('routeChangeStart', () => NProgress.start());
Router.events.on('routeChangeComplete', () => NProgress.done());
Router.events.on('routeChangeError', () => NProgress.done());

function MyApp({ Component, pageProps }) {
	return (
		<Provider session={pageProps.session}>
			<Layout>
				<Component {...pageProps} />
			</Layout>
		</Provider>
	);
}

export default MyApp;
