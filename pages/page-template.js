import { useState, useEffect } from 'react';
import Head from 'next/head';
import Banner from '../components/page-banners/HomepageBanner';

const PageTemplate = () => {
	return (
		<>
			<Head>
				<title>Peter Jones | Page Title</title>
			</Head>
			<Banner /> {/* optional */}
			<div className='content' style={{ height: '2000px' }}>
				<h1>A Page Template</h1>
				<p>Building out the page template</p>
			</div>
		</>
	);
};

export default PageTemplate;
