import Head from 'next/head';
import Banner from '../../components/page-banners/ContactBanner';

const Contact = () => {
	return (
		<>
			<Head>
				<title>Peter Jones | Contact</title>
			</Head>
			<Banner />
			<div className='content' style={{ minHeight: '2000px' }}>
				<h1>Contact</h1>
				<p>Contact form goes here...</p>
			</div>
		</>
	);
};

export default Contact;
