import Head from 'next/head';
import Link from 'next/link';
import Banner from '../../components/page-banners/KeyboardBanner';

const Code = () => {
	return (
		<>
			<Head>
				<title>Peter Jones | Code</title>
			</Head>
			<Banner />
			<div className='content' style={{ minHeight: '2000px' }}>
				<h1>Projects</h1>
				<p>This is where all of the projects go!</p>
				<ul>
					<li>
						<Link href='/projects/weather-app'>
							<a>Weather App</a>
						</Link>
					</li>
					<li>
						<Link href='/projects/random-password-generator'>
							<a>Password Generator</a>
						</Link>
					</li>
					<li>
						<Link href='/projects/pagination'>
							<a>Pagination</a>
						</Link>
					</li>
					<li>
						<Link href='/projects/checkbox-styling'>
							<a>Checkbox Styling</a>
						</Link>
					</li>
					<li>
						<Link href='/projects/pizza-pie'>
							<a>Pizza Pie</a>
						</Link>
					</li>
					<li>
						<Link href='/projects/rollup-counter'>
							<a>Javascript Rollup Counter</a>
						</Link>
					</li>
					<li>
						<Link href='/projects/js-clock'>
							<a>Twelve Hour Clock</a>
						</Link>
					</li>
				</ul>
			</div>
		</>
	);
};

export default Code;
