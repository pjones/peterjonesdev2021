import { useState, useEffect } from 'react';

const ContactBanner = () => {
	const [scroll, setScroll] = useState('');

	useEffect(() => {
		window.scrollTo(0, 0);
		window.addEventListener('scroll', handleScroll);
		return () => window.removeEventListener('scroll', handleScroll);
	}, []);

	function handleScroll() {
		let scroll = window.scrollY;
		setScroll(scroll);
	}

	return (
		<section id='hero-section'>
			<div className='banner'>
				<img
					src='/images/banners/People.webp'
					alt='banner'
					style={{
						transform:
							'translate3d(0, 0, 0)  scale(' + (100 + scroll / 5) / 100 + ')',
						objectPosition: 'top center'
					}}
				/>
			</div>
		</section>
	);
};

export default ContactBanner;
