import { useState, useEffect } from 'react';
import Link from 'next/link';
import Image from 'next/image';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faSun, faMoon, faBell } from '@fortawesome/free-solid-svg-icons';
import styles from '../styles/Navbar.module.scss';

library.add(faSun, faMoon, faBell);

const Navbar = () => {
	const [theme, setTheme] = useState('');

	useEffect(() => {
		const localTheme = localStorage.getItem('theme');
		setTheme(localTheme);

		if (localTheme === null || localTheme === '') {
			localStorage.setItem('theme', 'light');
			setTheme('light');
		} else if (localTheme === 'dark') {
			document.body.classList.add('dark');
		}
	});

	const toggleTheme = e => {
		if (theme === 'light') {
			localStorage.setItem('theme', 'dark');
			document.body.classList.add('dark');
			setTheme('dark');
		} else {
			localStorage.setItem('theme', 'light');
			document.body.classList.remove('dark');
			setTheme('light');
		}
	};

	return (
		<nav className={styles.nav}>
			<div className={styles.navContent}>
				<div className='logo'>
					{theme === 'light' ? (
						<Link href='/'>
							<a>
								<Image
									src='/images/my-logo.png'
									alt='logo'
									width={224}
									height={60}
								/>
							</a>
						</Link>
					) : (
						<Link href='/'>
							<a>
								<Image
									src='/images/my-logo-dark.png'
									alt='logo'
									width={224}
									height={60}
								/>
							</a>
						</Link>
					)}
				</div>
				<div className={styles.topNavLinks}>
					<Link href='/projects'>
						<a>Projects</a>
					</Link>
					<Link href='/news'>
						<a>News</a>
					</Link>
					<Link href='/contact'>
						<a>Contact</a>
					</Link>
					<button
						name='toggle theme'
						title='Toggle Theme'
						aria-label='Toggle Theme'
						className={styles.faIcon}
						onClick={toggleTheme}
					>
						{theme === 'light' ? (
							<FontAwesomeIcon icon={faSun} />
						) : (
							<FontAwesomeIcon icon={faMoon} />
						)}
					</button>
				</div>
			</div>
		</nav>
	);
};

export default Navbar;
